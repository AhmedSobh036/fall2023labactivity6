//Ahmed Sobh
//Student Id: 2134222
package linearalgebra;
import java.lang.Math;
/**
 * Vector3d is a class used for assignment 6 ungraded lab
 * @author Ahmed Sobh
 * @version 10/4/2023
 */
public class Vector3d{

    private double x;
    private double y;
    private double z;
    /**
     * Constructor for the class Vector3d
     */
    public Vector3d (double x, double y, double z){

        this.x = x;
        this.y = y;
        this.z = z;

    }
    /**
     * gets x
     * @return double the value of x
     */
    public double getX(){
        return this.x;
    }
    /**
     * gets y
     * @return double the value of y
     */
    public double getY(){
        return this.y;
    }
    /**
     * gets the value z
     * @return double the value of z
     */
    public double getZ(){
        return this.z;
    }
    /**
     * calculates the magnitude
     * @return double Returns the result of the calculation
     */
    public double magnitude(){
        return Math.sqrt((Math.pow(x,2))+(Math.pow(y,2))+(Math.pow(z,2)));
    }
    /**
     * calculates the dot product of two vectors
     * @param Vector3d the vector we want to multiply
     * @return double the product of the two functions
     */
    public double dotProduct(Vector3d secondVector){
        return (this.x * secondVector.getX()) + (this.y * secondVector.getY()) + (this.z * secondVector.getZ());
    }
    /**
     * add two vectors
     * @param Vector3d the vector we want to add with 
     * @return returns a double which is the sum of both vectors
     */
    public Vector3d add(Vector3d secondVector){
        Vector3d returnedVector = new Vector3d(this.x + secondVector.getX(), this.y + secondVector.getY(), this.z + secondVector.getZ());
        return returnedVector;
    }
    /**
     * main method 
     */
    public static void main(String args[]){
        Vector3d firstVector = new Vector3d(1,2,3);
        Vector3d secondVector = new Vector3d(4,5,6);

        double mag = firstVector.magnitude();
        System.out.println(mag);

        double dotProduct = firstVector.dotProduct(secondVector);
        System.out.println(dotProduct);

        Vector3d testVector = new Vector3d(1,2,3);
        testVector = firstVector.add(secondVector);

        double testX = testVector.getX();
        double testY = testVector.getY();
        double testZ = testVector.getZ();

        System.out.println(testX + " " + testY + " " + testZ);
    }

}